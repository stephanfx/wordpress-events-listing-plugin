<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Events_Listing
 *
 * @wordpress-plugin
 * Plugin Name:       Events Listing
 * Plugin URI:        http://example.com/events-listing-uri/
 * Description:       Very Simple plugin to list events on your site.
 * Version:           1.0.0
 * Author:            Stephan Grobler
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       events-listing
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-events-listing-activator.php';

/**
 * The code that runs during plugin deactivation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-events-listing-deactivator.php';

/** This action is documented in includes/class-events-listing-activator.php */
register_activation_hook( __FILE__, array( 'Events_Listing_Activator', 'activate' ) );

/** This action is documented in includes/class-events-listing-deactivator.php */
register_deactivation_hook( __FILE__, array( 'Events_Listing_Deactivator', 'deactivate' ) );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-events-listing.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_events_listing() {

	$plugin = new Events_Listing();
	$plugin->run();

}
run_events_listing();
