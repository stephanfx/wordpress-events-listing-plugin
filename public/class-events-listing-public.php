<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Events_Listing
 * @subpackage Events_Listing/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Events_Listing
 * @subpackage Events_Listing/public
 * @author     Your Name <email@example.com>
 */
class Events_Listing_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $events_listing    The ID of this plugin.
	 */
	private $events_listing;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $events_listing       The name of the plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $events_listing, $version ) {

		$this->events_listing = $events_listing;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Events_Listing_Public_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Events_Listing_Public_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->events_listing, plugin_dir_url( __FILE__ ) . 'css/events-listing-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Events_Listing_Public_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Events_Listing_Public_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->events_listing, plugin_dir_url( __FILE__ ) . 'js/events-listing-public.js', array( 'jquery' ), $this->version, false );

	}

}
