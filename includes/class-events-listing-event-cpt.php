<?php

/**
 * Service Custom Post Type
 */
class Events_Listing_Event_CustomPostType
{

	function __construct($post_id = null)
	{
		if (!empty($post_id)) {
			$this->getPost($post_id);
		}
	}

	public function getPost($post_id)
	{ }

	public function addMetaBoxes()
	{
		add_meta_box(
			'event_meta',
			'Additional Event Information',
			array('Events_Listing_Event_CustomPostType', 'getMetaBox'),
			'event',
			'normal'
		);
	}

	public function getMetaBox($post)
	{
		$telephone_nr = get_post_meta($post->ID, '_event_telephone_nr', 1);
		$fax_nr = get_post_meta($post->ID, '_event_fax_nr', 1);
		$email_address = get_post_meta($post->ID, '_event_email_address', 1);
		$website_address = get_post_meta($post->ID, '_event_website_address', 1);
		$physical_address = get_post_meta($post->ID, '_event_physical_address', 1);
		$from_date = get_post_meta($post->ID, '_event_from_date', 1);
		$to_date = get_post_meta($post->ID, '_event_to_date', 1);


		include_once plugin_dir_path(__FILE__) . '../admin/partials/events-listing-event-meta-box.php';
	}

	public function apiInit($server)
	{
		global $events_listing_event_api;

		$events_listing_event_api = new Events_Listing_Event_API($server);
		$events_listing_event_api->register_filters();
	}

	public function savePost($post_id)
	{
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}

		if (isset($_POST['post_type']) && 'service' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id)) {
				return $post_id;
			}
			update_post_meta($post_id, '_event_telephone_nr', sanitize_text_field($_POST['telephone_nr']));
			update_post_meta($post_id, '_event_fax_nr', sanitize_text_field($_POST['fax_nr']));
			update_post_meta($post_id, '_event_email_address', sanitize_email($_POST['email_address']));
			update_post_meta($post_id, '_event_website_address', esc_url($_POST['website_address']));
			update_post_meta($post_id, '_event_physical_address', sanitize_text_field($_POST['physical_address']));
			update_post_meta($post_id, '_event_from_date', sanitize_text_field($_POST['from_date']));
			update_post_meta($post_id, '_event_to_date', sanitize_text_field($_POST['to_date']));
		} else {
			if (!current_user_can('edit_post', $post_id)) {
				return $post_id;
			}
		}
	}

	public function init()
	{
		register_post_type(
			'event',
			array(
				'labels' => array(
					'name' => __('Events'),
					'singular_name' => __('Event'),
				),
				'public' => true,
				'has_archive' => true,
				'supports' => array(
					'title',
					'editor',
					'excerpt',
					'thumbnail'
				),
			)
		);
	}
}
